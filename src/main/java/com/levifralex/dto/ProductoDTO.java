package com.levifralex.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Producto DTO Data")
public class ProductoDTO {

	private Integer idProducto;

	@Schema(description = "Nombre del producto")
	@NotNull(message = "{nombre.notNull}")
	@Size(min = 3, message = "{nombre.size}")
	private String nombre;

	@Schema(description = "Marca del producto")
	@NotNull(message = "{marca.notNull}")
	@Size(min = 3, message = "{marca.size}")
	private String marca;

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

}
