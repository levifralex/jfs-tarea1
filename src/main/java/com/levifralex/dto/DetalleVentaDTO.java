package com.levifralex.dto;

import javax.validation.constraints.NotNull;

public class DetalleVentaDTO {

	private Integer idDetalleVenta;

	private VentaDTO venta;

	@NotNull(message = "{producto.notNull}")
	private ProductoDTO producto;

	@NotNull(message = "{cantidad.notNull}")
	private Integer cantidad;

	public Integer getIdDetalleVenta() {
		return idDetalleVenta;
	}

	public void setIdDetalleVenta(Integer idDetalleVenta) {
		this.idDetalleVenta = idDetalleVenta;
	}

	public VentaDTO getVenta() {
		return venta;
	}

	public void setVenta(VentaDTO venta) {
		this.venta = venta;
	}

	public ProductoDTO getProducto() {
		return producto;
	}

	public void setProducto(ProductoDTO producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

}
