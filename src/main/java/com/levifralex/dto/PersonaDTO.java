package com.levifralex.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Persona DTO Data")
public class PersonaDTO {

	private Integer idPersona;

	@Schema(description = "Nombres de la persona")
	@NotNull(message = "{nombres.notNull}")
	@Size(min = 3, message = "{nombres.size}")
	private String nombres;

	@Schema(description = "Apellidos de la persona")
	@NotNull(message = "{nombres.notNull}")
	@Size(min = 3, message = "{apellidos.size}")
	private String apellidos;

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

}
