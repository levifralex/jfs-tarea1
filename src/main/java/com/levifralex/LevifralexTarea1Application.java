package com.levifralex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LevifralexTarea1Application {

	public static void main(String[] args) {
		SpringApplication.run(LevifralexTarea1Application.class, args);
	}

}
