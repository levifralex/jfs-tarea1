package com.levifralex.controller;

import java.net.URI;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.levifralex.dto.VentaDTO;
import com.levifralex.model.Venta;
import com.levifralex.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService service;

	@Autowired
	private ModelMapper mapper;

	@PostMapping
	public ResponseEntity<Void> registrar(@Valid @RequestBody VentaDTO dto) throws Exception {
		Venta c = mapper.map(dto, Venta.class);

		Venta obj = service.registrarTransaccional(c);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdVenta())
				.toUri();
		return ResponseEntity.created(location).build();
	}

}
