package com.levifralex.repo;

import com.levifralex.model.Persona;

public interface IPersonaRepo extends IGenericRepo<Persona, Integer>{

}
