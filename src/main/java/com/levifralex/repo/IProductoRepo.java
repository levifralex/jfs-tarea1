package com.levifralex.repo;

import com.levifralex.model.Producto;

public interface IProductoRepo extends IGenericRepo<Producto, Integer>{

}
