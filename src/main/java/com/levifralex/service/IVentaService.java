package com.levifralex.service;

import com.levifralex.model.Venta;

public interface IVentaService extends ICRUD<Venta, Integer> {

	Venta registrarTransaccional(Venta consulta) throws Exception;

}
